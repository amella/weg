﻿
Imports System.Xml

Public Class WegUpdateService

    Private timer As System.Threading.Timer
    Dim callback As System.Threading.TimerCallback

    Dim archivos As List(Of Archivo) = New List(Of Archivo)
    Dim mailSettings As MailSettings = Nothing
    Dim empresa As Empresa = Nothing
    Dim archivo As Archivo = Nothing
    Dim credencial As Credencial = Nothing
    Dim tiempo As Tiempo = Nothing
    Dim logDir As String = Nothing

    Protected Overrides Sub OnStart(ByVal args() As String)

        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        Try

            Me.loadConf()

        Catch ex As Exception

            EventLog1.WriteEntry("Timer no agendado")

            Return

        End Try

        Me.setTimer()

    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Me.timer.Dispose()
    End Sub

    Public Sub New()

        InitializeComponent()

        ' Turn off autologging 
        Me.AutoLog = False
        ' Create a new event source and specify a log name that 
        ' does not exist to create a custom log 
        If Not System.Diagnostics.EventLog.SourceExists("WegUpdater") Then
            System.Diagnostics.EventLog.CreateEventSource("WegUpdater", "WegUpdaterLog")
        End If
        ' Configure the event log instance to use this source name
        EventLog1.Source = "WegUpdater"

    End Sub

    Private Sub loadConf()

        Try

            Dim doc As XmlDocument = New XmlDocument()            
            doc.Load("G:\Aspel\dacaspel\weg_conf.xml")

            '1.- Seleccionar todas los archivos
            Dim nodeList As XmlNodeList = doc.SelectNodes("//archivos/archivo")
            Dim node As XmlNode
            Dim childNode As XmlNode

            For Each node In nodeList

                archivo = New Archivo()

                For Each childNode In node.ChildNodes

                    If childNode.Name = "empresas" Then

                        Dim empresas = childNode.ChildNodes
                        Dim empresaNode As XmlNode

                        For Each empresaNode In empresas

                            empresa = New Empresa()

                            If empresaNode.Name = "empresa" Then

                                Dim nodes = empresaNode.ChildNodes

                                For Each w As XmlNode In nodes

                                    If w.Name = "id" Then
                                        Try

                                            Dim _empresa As Integer = Integer.Parse(w.InnerText.Trim())

                                            If _empresa < 10 Then
                                                empresa._id = "0" & _empresa
                                            Else
                                                empresa._id = _empresa
                                            End If

                                        Catch ex As Exception

                                            Console.WriteLine(ex.Message)

                                            Throw ex

                                        End Try

                                    ElseIf w.Name = "esquema" Then

                                        empresa._esquema = w.InnerText

                                    End If

                                Next

                            End If

                            archivo._empresas.Add(empresa)

                        Next


                    ElseIf childNode.Name = "ubicacion" Then

                        archivo._ubicacion = childNode.InnerText

                    ElseIf childNode.Name = "host" Then

                        archivo._host = childNode.InnerText

                    End If

                Next

                archivos.Add(archivo)

            Next

            '3.- Seleccionar todas la credencial
            node = doc.SelectSingleNode("//credencial")

            credencial = New Credencial()

            For Each childNode In node

                If childNode.Name = "usuario" Then

                    credencial._usuario = childNode.InnerText

                ElseIf childNode.Name = "paswword" Then

                    credencial._password = childNode.InnerText

                End If

            Next

            '4.- Seleccionar la hora
            node = doc.SelectSingleNode("//tiempo")

            tiempo = New Tiempo()

            For Each childNode In node

                If childNode.Name = "hora" Then

                    tiempo._hora = childNode.InnerText

                ElseIf childNode.Name = "minuto" Then

                    tiempo._minuto = childNode.InnerText

                End If

            Next

            node = doc.SelectSingleNode("//log")
            logDir = node.InnerText

            'mail

            node = doc.SelectSingleNode("//mail")

            mailSettings = New MailSettings()

            For Each childNode In node

                If childNode.Name = "from" Then

                    mailSettings._from = childNode.InnerText

                ElseIf childNode.Name = "tos" Then

                    Dim tos = childNode.ChildNodes
                    Dim toNode As XmlNode

                    For Each toNode In tos

                        mailSettings._tos.Add(toNode.InnerText)

                    Next

                ElseIf childNode.Name = "host" Then

                    mailSettings._host = childNode.InnerText
                ElseIf childNode.Name = "port" Then

                    mailSettings._port = childNode.InnerText
                ElseIf childNode.Name = "user" Then

                    mailSettings._user = childNode.InnerText
                ElseIf childNode.Name = "password" Then

                    mailSettings._password = childNode.InnerText
                ElseIf childNode.Name = "timeout" Then

                    mailSettings._timeout = childNode.InnerText
                ElseIf childNode.Name = "subject" Then

                    mailSettings._subject = childNode.InnerText
                End If

            Next

        Catch ex As Exception

            Functions.writeToError("loadConf::Exception::message::" & ex.Message)

            Throw ex

        End Try

    End Sub

    Private Function getTrabajadoresPorEmpresa(ByRef trabajadores As List(Of Trabajador), ByRef id As String)

        Dim foo As List(Of Trabajador) = New List(Of Trabajador)

        For Each trabajador As Trabajador In trabajadores

            Dim clave As Long = Long.Parse(trabajador._CLAVE)

            If "01".Equals(id) And 5 <= clave And clave <= 4999 Then

                'Empresa 01 rango 5 a 4999
                trabajador._CLASIF = "CONF"
                foo.Add(trabajador)

            ElseIf "02".Equals(id) And 5000 <= clave And clave <= 29999 Then

                'Empresa 02 rango 5000 a 30000
                trabajador._CLASIF = "SIND"
                foo.Add(trabajador)

            ElseIf "03".Equals(id) And 20000 <= clave And clave <= 29999 Then

                'Empresa 03 rango 20000 a 29999
                trabajador._CLASIF = "CONF"
                foo.Add(trabajador)

            ElseIf "04".Equals(id) And 30000 <= clave And clave <= 49999 Then

                'Empresa 04 rango 30000 a 50000
                trabajador._CLASIF = "SIND"
                foo.Add(trabajador)

            ElseIf "05".Equals(id) And 2000 <= clave And clave <= 9999 Then

                'Empresa 05 rango 2000 a 9999
                trabajador._CLASIF = "CONF"
                foo.Add(trabajador)

            ElseIf "06".Equals(id) And 1000 <= clave And clave <= 1999 Then

                'Empresa 06 rango 1000 a 1999 y 10000 a 30000
                trabajador._CLASIF = "SIND"
                foo.Add(trabajador)

            ElseIf "06".Equals(id) And 10000 <= clave And clave <= 29999 Then

                'Empresa 06 rango 1000 a 1999 y 10000 a 30000
                trabajador._CLASIF = "SIND"
                foo.Add(trabajador)

            End If

        Next

        Return foo

    End Function

    Public Function getFamilias(ByRef trabajadores As List(Of Trabajador))

        Dim familias As List(Of Familia) = New List(Of Familia)
        Dim familia As Familia = Nothing

        For Each trabajador As Trabajador In trabajadores

            familia = New Familia()
            familia._clave = trabajador._CLAVE
            familia._parentesco = 0
            familia._sexo = "M"
            familia._nombre = trabajador._NOM_PADRE

            familias.Add(familia)

            familia = New Familia()
            familia._clave = trabajador._CLAVE
            familia._parentesco = 1
            familia._sexo = "F"
            familia._nombre = trabajador._NOM_MADRE

            familias.Add(familia)

        Next

        Return familias

    End Function


    Private Sub setTimer()

        If Me.timer Is Nothing Then

            Dim now As DateTime = DateTime.Now()

            Dim today12am = New DateTime(now.Year, now.Month, now.Day, Me.tiempo._hora, Me.tiempo._minuto, 0)

            Dim next12am As DateTime

            If now <= today12am Then
                next12am = today12am
            Else
                next12am = today12am.AddDays(1)
            End If

            Me.callback = New System.Threading.TimerCallback(AddressOf doUpdate)
            Dim timSpan As TimeSpan = next12am - DateTime.Now

            EventLog1.WriteEntry("Faltan " & timSpan.TotalHours & " hrs para ejecución.")

            Me.timer = New System.Threading.Timer(callback, Nothing, timSpan, TimeSpan.FromDays(1))

        End If

    End Sub

    Public Sub doUpdate()

        EventLog1.WriteEntry("Actualizando ")

        Dim trabajadorDAO As TrabajadorDAO = Nothing
        Dim connectionManager As ConnectionManager = Nothing
        Dim servicio As Servicio = New Servicio()
        servicio._log = logDir
        Dim trabajadores As List(Of Trabajador) = Nothing

        For Each archivo As Archivo In archivos

            servicio = New Servicio()
            servicio._strRoute = archivo._ubicacion

            'Se obtienen todos los trabajadores del archivo
            trabajadores = servicio.getTrabajadores()

            archivo._totalUpdate = trabajadores.Count()

            For Each empresa As Empresa In archivo._empresas

                connectionManager = New ConnectionManager()
                connectionManager.Host = archivo._host
                connectionManager.Schema = empresa._esquema
                connectionManager.User = credencial._usuario
                connectionManager.Pass = credencial._password

                trabajadorDAO = New TrabajadorDAO()
                trabajadorDAO._conectionManager = connectionManager
                trabajadorDAO._empresa = empresa._id
                trabajadorDAO.getActualDate()

                servicio._trabajadorDAO = trabajadorDAO

                Try

                    'Cuáles son mis trabajadores
                    Dim misTrabajadores As List(Of Trabajador) = getTrabajadoresPorEmpresa(trabajadores, empresa._id)

                    servicio.getDeptos(misTrabajadores)
                    'Obtener las claves correctas
                    servicio.getCorrectClaves(misTrabajadores)

                    'Cuáles son nuevos
                    Dim nuevos As List(Of Trabajador) = servicio.getTrabajadoresNuevos(misTrabajadores)
                    'Cuales ya existen
                    Dim existentes As List(Of Trabajador) = servicio.getTrabajadoresExistentes(misTrabajadores)

                    empresa._totalUpdate = existentes.Count()
                    empresa._totalInsert = nuevos.Count()
                    empresa._total = misTrabajadores.Count()
                    empresa._actualDateEmpresa = servicio._trabajadorDAO._actualDateEmpresa

                    servicio.doUpdateTrabajadores(existentes)
                    servicio.doInsertTrabajadores(nuevos)

                    Dim familias As List(Of Familia) = getFamilias(misTrabajadores)

                    'Cuáles son nuevas
                    Dim nuevosFamilia As List(Of Familia) = servicio.getFamiliasNuevos(familias)
                    'Cuáles existen
                    Dim existentesFamilia As List(Of Familia) = servicio.getFamiliasExistentes(familias)

                    servicio.doUpdateFamilias(existentesFamilia)
                    servicio.doInsertFamilias(nuevosFamilia)

                Catch ex As Exception

                    Functions.writeToError("loadConf::Exception::message::" & ex.Message)

                End Try

            Next

        Next

        servicio._now = DateTime.Now()

        servicio.writeToFile(archivos, mailSettings)

        servicio.sendmail(archivos, mailSettings)

    End Sub

End Class
