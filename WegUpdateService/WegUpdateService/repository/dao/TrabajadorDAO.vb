﻿Imports FirebirdSql.Data.FirebirdClient

Public Class TrabajadorDAO

    Public _conectionManager As ConnectionManager
    Public _actualDateEmpresa As String
    Public _empresa As String

    Private Function getActualDateQuery()

        Dim foo As String = " select first 1 substring(rdb$relation_name from 3 for 8 ), "
        foo = foo & " substring(rdb$relation_name from 3 for 2 ) as d, "
        foo = foo & " substring(rdb$relation_name from 5 for 2 ) as m, "
        foo = foo & " substring(rdb$relation_name from 7 for 2 ) as y  "
        foo = foo & " from rdb$relations where trim(rdb$relation_name) "       
        foo = foo & " similar to 'TB[[:DIGIT:]]{5,6}" & _empresa & "' order by y desc, m desc, d desc "

        Return foo

    End Function

    Private Function getDeptoQuery(ByRef foo As Trabajador)

        Dim w As String = "select CLAVE from DEPTOS" & _empresa & " where CUENTA_COI='" & foo._CUENTA_COI & "'"

        Return w

    End Function

    Public Function getDepto(ByRef trabajador As Trabajador)

        Dim sqlConnection As FbConnection = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim fbDataReader As FbDataReader = Nothing

        Dim existe As Boolean = False
        Dim depto As String = ""

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()

            fbCommand = sqlConnection.CreateCommand()

            fbCommand.CommandText = Me.getDeptoQuery(trabajador)
            fbDataReader = fbCommand.ExecuteReader()

            While fbDataReader.Read()

                depto = fbDataReader(0)

            End While

        Catch ex As Exception

            Functions.writeToError("getDepto::Exception::message::" & ex.Message)

            depto = -1

            Throw ex

        Finally

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

        Return depto

    End Function

    Private Function getStringDateFromDate(ByRef foo As Date)

        Dim fecha As String = Nothing

        If foo <> Nothing Then

            fecha = "" & foo.Year & "-" & foo.Month & "-" & foo.Day & ""

        End If

        Return fecha

    End Function

    Private Function getTrabajadorUpdateString(ByRef t As Trabajador)

        Dim foo As String
        Dim fooDate As String = Nothing

        foo = "UPDATE TB" & Me._actualDateEmpresa & " SET "

        If t._DEPTO <> Nothing And t._DEPTO <> Constants.EMPTY_STR Then
            foo = foo & " DEPTO='" & t._DEPTO & "', "
        End If

        If t._CLASIF <> Nothing And t._CLASIF <> Constants.EMPTY_STR Then
            foo = foo & " CLASIF='" & t._CLASIF & "', "
        End If

        If t._NOMBRE <> Nothing And t._NOMBRE <> Constants.EMPTY_STR Then
            foo = foo & " NOMBRE='" & t._NOMBRE & "', "
        End If

        If t._AP_PAT_ <> Nothing And t._AP_PAT_ <> Constants.EMPTY_STR Then
            foo = foo & " AP_PAT_='" & t._AP_PAT_ & "', "
        End If

        If t._AP_MAT_ <> Nothing And t._AP_MAT_ <> Constants.EMPTY_STR Then
            foo = foo & " AP_MAT_='" & t._AP_MAT_ & "', "
        End If

        If t._FECH_ALTA <> Nothing Then

            foo = foo & " FECH_ALTA='" & Me.getStringDateFromDate(t._FECH_ALTA) & "', "

        End If

        If t._STATUS <> Nothing And t._STATUS <> Constants.EMPTY_STR Then
            foo = foo & " STATUS='" & t._STATUS & "', "
        End If

        If t._IMSS <> Nothing And t._IMSS <> Constants.EMPTY_STR Then
            foo = foo & " IMSS='" & t._IMSS & "', "
        End If

        If t._CURP <> Nothing And t._CURP <> Constants.EMPTY_STR Then
            foo = foo & " CURP='" & t._CURP & "', "
        End If

        If t._SEXO <> Nothing And t._SEXO <> Constants.EMPTY_STR Then
            foo = foo & " SEXO='" & t._SEXO & "', "
        End If

        If t._PUESTO <> Nothing And t._PUESTO <> Constants.EMPTY_STR Then
            foo = foo & " PUESTO='" & t._PUESTO & "', "
        End If

        If t._PROFESION <> Nothing And t._PROFESION <> Constants.EMPTY_STR Then
            foo = foo & " PROFESION='" & t._PROFESION & "', "
        End If

        If t._FECH_NACIM <> Nothing Then

            foo = foo & " FECH_NACIM='" & Me.getStringDateFromDate(t._FECH_NACIM) & "', "

        End If

        If t._CTACHEQNOM <> Nothing And t._CTACHEQNOM <> Constants.EMPTY_STR Then
            foo = foo & " CTACHEQNOM='" & t._CTACHEQNOM & "', "
        End If

        If t._ENT_FED <> Nothing And t._ENT_FED <> Constants.EMPTY_STR Then
            foo = foo & " ENT_FED='" & t._ENT_FED & "', "
        End If

        If t._CD_POBLAC <> Nothing And t._CD_POBLAC <> Constants.EMPTY_STR Then
            foo = foo & " CD_POBLAC='" & t._CD_POBLAC & "', "
        End If

        If t._COLONIA <> Nothing And t._COLONIA <> Constants.EMPTY_STR Then
            foo = foo & " COLONIA='" & t._COLONIA & "', "
        End If

        If t._COD_POST <> Nothing And t._COD_POST <> Constants.EMPTY_STR Then
            foo = foo & " COD_POST='" & t._COD_POST & "', "
        End If

        If t._CALLE <> Nothing And t._CALLE <> Constants.EMPTY_STR Then
            foo = foo & " CALLE='" & t._CALLE & "', "
        End If

        If t._FECH_BAJA <> Nothing Then

            foo = foo & " FECH_BAJA='" & Me.getStringDateFromDate(t._FECH_BAJA) & "', "

        End If

        If t._TELEFONO <> Nothing And t._TELEFONO <> Constants.EMPTY_STR Then
            foo = foo & " TELEFONO='" & t._TELEFONO & "', "
        End If

        If t._EMAIL <> Nothing And t._EMAIL <> Constants.EMPTY_STR Then
            foo = foo & " EMAIL='" & t._EMAIL & "', "
        End If

        If t._LUG_NACIM <> Nothing And t._LUG_NACIM <> Constants.EMPTY_STR Then
            foo = foo & " LUG_NACIM='" & t._LUG_NACIM & "', "
        End If

        If t._SDI <> Nothing Then
            foo = foo & " SDI=" & t._SDI & ", "
        End If

        If t._SAL_DIARIO <> Nothing Then
            foo = foo & " SAL_DIARIO=" & t._SAL_DIARIO & ", "
        End If

        foo = foo.Substring(0, Len(foo) - 2)

        foo = foo & " WHERE CLAVE='" & t._CLAVE & "'"

        Return foo

    End Function

    Private Function getTrabajadorInsertString(ByRef t As Trabajador)

        Dim foo As String
        Dim fooDate As String = Nothing

        foo = "INSERT INTO TB" & Me._actualDateEmpresa & " (CLASIF, CLAVE,"

        If t._DEPTO <> Nothing And t._DEPTO <> Constants.EMPTY_STR Then
            foo = foo & " DEPTO, "
        End If

        If t._NOMBRE <> Nothing And t._NOMBRE <> Constants.EMPTY_STR Then
            foo = foo & " NOMBRE, "
        End If

        If t._AP_PAT_ <> Nothing And t._AP_PAT_ <> Constants.EMPTY_STR Then
            foo = foo & " AP_PAT_, "
        End If

        If t._AP_MAT_ <> Nothing And t._AP_MAT_ <> Constants.EMPTY_STR Then
            foo = foo & " AP_MAT_, "
        End If

        If t._FECH_ALTA <> Nothing Then

            foo = foo & " FECH_ALTA, "

        End If

        If t._STATUS <> Nothing And t._STATUS <> Constants.EMPTY_STR Then
            foo = foo & " STATUS, "
        End If

        If t._IMSS <> Nothing And t._IMSS <> Constants.EMPTY_STR Then
            foo = foo & " IMSS, "
        End If

        If t._CURP <> Nothing And t._CURP <> Constants.EMPTY_STR Then
            foo = foo & " CURP, "
        End If

        If t._SEXO <> Nothing And t._SEXO <> Constants.EMPTY_STR Then
            foo = foo & " SEXO, "
        End If

        If t._PUESTO <> Nothing And t._PUESTO <> Constants.EMPTY_STR Then
            foo = foo & " PUESTO, "
        End If

        If t._PROFESION <> Nothing And t._PROFESION <> Constants.EMPTY_STR Then
            foo = foo & " PROFESION, "
        End If

        If t._FECH_NACIM <> Nothing Then

            foo = foo & " FECH_NACIM, "

        End If

        If t._CTACHEQNOM <> Nothing And t._CTACHEQNOM <> Constants.EMPTY_STR Then
            foo = foo & " CTACHEQNOM, "
        End If

        If t._ENT_FED <> Nothing And t._ENT_FED <> Constants.EMPTY_STR Then
            foo = foo & " ENT_FED, "
        End If

        If t._CD_POBLAC <> Nothing And t._CD_POBLAC <> Constants.EMPTY_STR Then
            foo = foo & " CD_POBLAC, "
        End If

        If t._COLONIA <> Nothing And t._COLONIA <> Constants.EMPTY_STR Then
            foo = foo & " COLONIA, "
        End If

        If t._COD_POST <> Nothing And t._COD_POST <> Constants.EMPTY_STR Then
            foo = foo & " COD_POST, "
        End If

        If t._CALLE <> Nothing And t._CALLE <> Constants.EMPTY_STR Then
            foo = foo & " CALLE, "
        End If

        If t._FECH_BAJA <> Nothing Then

            foo = foo & " FECH_BAJA, "

        End If

        If t._TELEFONO <> Nothing And t._TELEFONO <> Constants.EMPTY_STR Then
            foo = foo & " TELEFONO, "
        End If

        If t._EMAIL <> Nothing And t._EMAIL <> Constants.EMPTY_STR Then
            foo = foo & " EMAIL, "
        End If

        If t._LUG_NACIM <> Nothing And t._LUG_NACIM <> Constants.EMPTY_STR Then
            foo = foo & " LUG_NACIM, "
        End If

        If t._SDI <> Nothing Then
            foo = foo & " SDI, "
        End If

        If t._SAL_DIARIO <> Nothing Then
            foo = foo & " SAL_DIARIO, "
        End If

        foo = foo.Substring(0, Len(foo) - 2)

        foo = foo & " ) values ( '" & t._CLASIF & "', '" & t._CLAVE & "',"

        If t._DEPTO <> Nothing And t._DEPTO <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._DEPTO & "', "
        End If

        If t._NOMBRE <> Nothing And t._NOMBRE <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._NOMBRE & "', "
        End If

        If t._AP_PAT_ <> Nothing And t._AP_PAT_ <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._AP_PAT_ & "', "
        End If

        If t._AP_MAT_ <> Nothing And t._AP_MAT_ <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._AP_MAT_ & "', "
        End If

        If t._FECH_ALTA <> Nothing Then

            foo = foo & "'" & Me.getStringDateFromDate(t._FECH_ALTA) & "', "

        End If

        If t._STATUS <> Nothing And t._STATUS <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._STATUS & "', "
        End If

        If t._IMSS <> Nothing And t._IMSS <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._IMSS & "', "
        End If

        If t._CURP <> Nothing And t._CURP <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._CURP & "', "
        End If

        If t._SEXO <> Nothing And t._SEXO <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._SEXO & "', "
        End If

        If t._PUESTO <> Nothing And t._PUESTO <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._PUESTO & "', "
        End If

        If t._PROFESION <> Nothing And t._PROFESION <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._PROFESION & "', "
        End If

        If t._FECH_NACIM <> Nothing Then

            foo = foo & "'" & Me.getStringDateFromDate(t._FECH_NACIM) & "', "

        End If

        If t._CTACHEQNOM <> Nothing And t._CTACHEQNOM <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._CTACHEQNOM & "', "
        End If

        If t._ENT_FED <> Nothing And t._ENT_FED <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._ENT_FED & "', "
        End If

        If t._CD_POBLAC <> Nothing And t._CD_POBLAC <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._CD_POBLAC & "', "
        End If

        If t._COLONIA <> Nothing And t._COLONIA <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._COLONIA & "', "
        End If

        If t._COD_POST <> Nothing And t._COD_POST <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._COD_POST & "', "
        End If

        If t._CALLE <> Nothing And t._CALLE <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._CALLE & "', "
        End If

        If t._FECH_BAJA <> Nothing Then

            foo = foo & "'" & Me.getStringDateFromDate(t._FECH_BAJA) & "', "

        End If

        If t._TELEFONO <> Nothing And t._TELEFONO <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._TELEFONO & "', "
        End If

        If t._EMAIL <> Nothing And t._EMAIL <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._EMAIL & "', "
        End If

        If t._LUG_NACIM <> Nothing And t._LUG_NACIM <> Constants.EMPTY_STR Then
            foo = foo & "'" & t._LUG_NACIM & "', "
        End If

        If t._SDI <> Nothing Then
            foo = foo & t._SDI & ", "
        End If

        If t._SAL_DIARIO <> Nothing Then
            foo = foo & t._SAL_DIARIO & ", "
        End If

        foo = foo.Substring(0, Len(foo) - 2)

        foo = foo & ")"

        Return foo

    End Function

    Private Function existTrabajadorQuery(ByRef t As Trabajador)

        Dim foo As String = "select count(*) from TB" & Me._actualDateEmpresa & " WHERE CLAVE='" & t._CLAVE & "'"

        Return foo

    End Function


    Private Function getFamiliaUpdateString(ByRef t As Familia)

        Dim foo As String = Nothing

        Dim name As String = t._nombre

        If t._nombre Is Nothing Then
            name = Constants.EMPTY_STR
        End If

        foo = "UPDATE FAMILIA" & Me._actualDateEmpresa.Substring(Len(Me._actualDateEmpresa) - 2) & " SET NOMBRE ='" & name & "' WHERE CLAVE_TRAB ='" & t._clave & "' AND PARENTESCO=" & t._parentesco

        Return foo

    End Function


    Private Function existFamiliaQuery(ByRef familia As Familia)

        Dim foo As String = "select count(*) from FAMILIA" & Me._actualDateEmpresa.Substring(Len(Me._actualDateEmpresa) - 2) & " WHERE CLAVE_TRAB ='" & familia._clave & "' AND PARENTESCO=" & familia._parentesco

        Return foo

    End Function


    Private Function getFamiliaInsertString(ByRef t As Familia)

        Dim foo As String = Nothing

        Dim name As String = t._nombre

        If t._nombre Is Nothing Then
            name = Constants.EMPTY_STR
        End If

        foo = "INSERT INTO FAMILIA" & Me._actualDateEmpresa.Substring(Len(Me._actualDateEmpresa) - 2) & "(SEXO,CONTADOR,NOMBRE,CLAVE_TRAB,PARENTESCO) values('" & t._sexo & "'," & "(select coalesce(MAX(CONTADOR),0,0)+1 from FAMILIA" & Me._actualDateEmpresa.Substring(Len(Me._actualDateEmpresa) - 2) & " where clave_trab='" & t._clave & "')" & ",'" & name & "','" & t._clave & "'," & t._parentesco & ")"

        Return foo

    End Function

    Private Function getCorrectClaveQuery(ByRef t As Trabajador)

        Dim foo As String = "select clave from TB" & Me._actualDateEmpresa & " WHERE cast(clave as integer)=" & Long.Parse(t._CLAVE)

        Return foo

    End Function

    Public Function getCorrectClave(ByRef trabajador As Trabajador)

        Dim sqlConnection As FbConnection = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim fbDataReader As FbDataReader = Nothing

        Dim existe As Boolean = False
        Dim cuenta As String = ""

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()

            fbCommand = sqlConnection.CreateCommand()

            fbCommand.CommandText = Me.getCorrectClaveQuery(trabajador)
            fbDataReader = fbCommand.ExecuteReader()

            Dim foo As Boolean = fbDataReader.Read()

            cuenta = trabajador._CLAVE

            If foo = True Then

                cuenta = fbDataReader(0)

                If cuenta <> Constants.EMPTY_STR Then

                    cuenta = cuenta.Trim()
                    cuenta = cuenta.PadLeft(10)

                End If

            End If

        Catch ex As Exception

            Functions.writeToError("getCorrectClave::Exception::message::" & ex.Message)

            cuenta = ""

            Throw ex

        Finally

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

        Return cuenta

    End Function

    Private Function existTrabajador(ByRef trabajador As Trabajador)

        Dim sqlConnection As FbConnection = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim fbDataReader As FbDataReader = Nothing

        Dim existe As Boolean = False
        Dim cuenta As Integer = -1

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()

            fbCommand = sqlConnection.CreateCommand()

            fbCommand.CommandText = Me.existTrabajadorQuery(trabajador)
            fbDataReader = fbCommand.ExecuteReader()

            While fbDataReader.Read()

                cuenta = fbDataReader(0)

            End While

        Catch ex As Exception

            Functions.writeToError("existTrabajador::Exception::message::" & ex.Message)

            cuenta = -1

            Throw ex

        Finally

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

        Return cuenta
    End Function

    Public Function getFamiliasNuevos(ByRef familias As List(Of Familia))

        Dim foo As List(Of Familia) = New List(Of Familia)

        For Each familia As Familia In familias

            If 0 = existFamilia(familia) Then
                foo.Add(familia)
            End If

        Next

        Return foo

    End Function

    Private Function existFamilia(ByRef t As Familia)

        Dim sqlConnection As FbConnection = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim fbDataReader As FbDataReader = Nothing

        Dim existe As Boolean = False
        Dim cuenta As Integer = -1

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()

            fbCommand = sqlConnection.CreateCommand()

            fbCommand.CommandText = Me.existFamiliaQuery(t)
            fbDataReader = fbCommand.ExecuteReader()

            While fbDataReader.Read()

                cuenta = fbDataReader(0)

            End While

        Catch ex As Exception

            Functions.writeToError("existFamilia::Exception::message::" & ex.Message)

            cuenta = -1

            Throw ex

        Finally

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

        Return cuenta
    End Function


    Public Function getFamiliasExistentes(ByRef familias As List(Of Familia))

        Dim foo As List(Of Familia) = New List(Of Familia)

        For Each familia As Familia In familias

            If 0 <> existFamilia(familia) Then
                foo.Add(familia)
            End If

        Next

        Return foo

    End Function

    Public Sub getActualDate()

        Dim sqlConnection As FbConnection = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim fbDataReader As FbDataReader = Nothing

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()

            fbCommand = sqlConnection.CreateCommand()

            fbCommand.CommandText = Me.getActualDateQuery()
            fbDataReader = fbCommand.ExecuteReader()

            While fbDataReader.Read()

                Me._actualDateEmpresa = fbDataReader(0)

            End While

        Catch ex As Exception

            Functions.writeToError("getActualDate::Exception::message::" & ex.Message)

            Throw ex

        Finally

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

    End Sub

    Public Function getTrabajadoresExistentes(ByRef trabajadores As List(Of Trabajador))

        Dim foo As List(Of Trabajador) = New List(Of Trabajador)

        For Each trabajador As Trabajador In trabajadores

            If existTrabajador(trabajador) <> 0 Then
                foo.Add(trabajador)
            End If

        Next

        Return foo

    End Function

    Public Function getTrabajadoresNuevos(ByRef trabajadores As List(Of Trabajador))

        Dim foo As List(Of Trabajador) = New List(Of Trabajador)

        For Each trabajador As Trabajador In trabajadores

            If 0 = existTrabajador(trabajador) Then
                foo.Add(trabajador)
            End If

        Next

        Return foo

    End Function

    Public Sub insertFamilias(ByRef familias As List(Of Familia))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlTransaction As FbTransaction = Nothing
        Dim fbCommand As FbCommand = Nothing

        'realizar insert

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()
            sqlTransaction = sqlConnection.BeginTransaction()

            fbCommand = sqlConnection.CreateCommand()
            fbCommand.Transaction = sqlTransaction

            For Each t As Familia In familias


                Dim foo As String = getFamiliaInsertString(t)

                fbCommand.CommandText = foo
                fbCommand.ExecuteNonQuery()

            Next

            sqlTransaction.Commit()

        Catch ex As Exception

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Rollback()
            End If

            Functions.writeToError("insertFamilias::Exception::message::" & ex.Message)

            Throw ex

        Finally

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Dispose()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

    End Sub

    Public Sub updateFamilias(ByRef familias As List(Of Familia))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlTransaction As FbTransaction = Nothing
        Dim fbCommand As FbCommand = Nothing

        'realizar insert

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()
            sqlTransaction = sqlConnection.BeginTransaction()

            fbCommand = sqlConnection.CreateCommand()
            fbCommand.Transaction = sqlTransaction

            For Each t As Familia In familias

                Dim foo As String = getFamiliaUpdateString(t)

                fbCommand.CommandText = foo
                fbCommand.ExecuteNonQuery()

            Next

            sqlTransaction.Commit()

        Catch ex As Exception

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Rollback()
            End If

            Functions.writeToError("insertFamilias::Exception::message::" & ex.Message)

            Throw ex

        Finally

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Dispose()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

    End Sub

    Public Sub insertTrabajadores(ByRef trabajadores As List(Of Trabajador))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlTransaction As FbTransaction = Nothing
        Dim fbCommand As FbCommand = Nothing

        'realizar insert

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()
            sqlTransaction = sqlConnection.BeginTransaction()

            fbCommand = sqlConnection.CreateCommand()
            fbCommand.Transaction = sqlTransaction

            For Each trabajador As Trabajador In trabajadores


                Dim foo As String = getTrabajadorInsertString(trabajador)

                fbCommand.CommandText = foo
                fbCommand.ExecuteNonQuery()

            Next

            sqlTransaction.Commit()

        Catch ex As Exception

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Rollback()
            End If

            Functions.writeToError("insertTrabajadores::Exception::message::" & ex.Message)

            Throw ex

        Finally

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Dispose()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

    End Sub


    Public Sub updateTrabajadores(ByRef trabajadores As List(Of Trabajador))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlTransaction As FbTransaction = Nothing
        Dim fbCommand As FbCommand = Nothing

        'realizar update
        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()
            sqlTransaction = sqlConnection.BeginTransaction()

            fbCommand = sqlConnection.CreateCommand()
            fbCommand.Transaction = sqlTransaction

            For Each trabajador As Trabajador In trabajadores

                Dim foo As String = getTrabajadorUpdateString(trabajador)
                fbCommand.CommandText = foo
                fbCommand.ExecuteNonQuery()

            Next

            sqlTransaction.Commit()

        Catch ex As Exception

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Rollback()
            End If

            Functions.writeToError("updateTrabajadores::Exception::message::" & ex.Message)

            Throw ex

        Finally

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Dispose()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

    End Sub

End Class
