﻿Public Class Trabajador

    Public _EXISTS As Boolean
    Public _CLAVE As String
    Public _NOMBRE As String
    Public _AP_PAT_ As String
    Public _AP_MAT_ As String
    Public _STATUS As String
    Public _R_F_C_ As String
    Public _DEPTO As String
    Public _PUESTO As String
    Public _IMSS As String
    Public _CLASIF As String
    Public _FECH_ALTA As Date
    Public _FECH_BAJA As Date
    Public _DIG_GRAV_ As String
    Public _FORM_PAGO As String
    Public _SAL_DIARIO As Double
    Public _SDI As String
    Public _SDI_INFO As Double
    Public _FECH_SAL As Date
    Public _TIP_SAL As String
    Public _CAMB_SDI As String
    Public _CALLE As String
    Public _COLONIA As String
    Public _CD_POBLAC As String
    Public _ENT_FED As String
    Public _COD_POST As String
    Public _BANC_NOM As Integer
    Public _LOCALI_NOM As String
    Public _CTRL_NOM As String
    Public _CTACHEQNOM As String
    Public _LUG_NACIM As String
    Public _MUNICIPIO As String
    Public _TIP_EMPL As String
    Public _CONTRATO As String
    Public _TURNO As String
    Public _EDO_CIVIL As String
    Public _UN_MED_FAM As Integer
    Public _TIPOSANGRE As String
    Public _BAND1 As String
    Public _BAND2 As String
    Public _PENS_ALIM As String
    Public _D_DESCANSO As String
    Public _CAUSABAJA As Integer
    Public _DISPONIBLE As String
    Public _VERSION As String
    Public _NOM_PADRE As String
    Public _NOM_MADRE As String
    Public _TELEFONO As String
    Public _SEXO As String
    Public _FECH_NACIM As Date
    Public _NUMFONACOT As String
    Public _TB_INTEG As Integer
    Public _CURP As String
    Public _FAL_DESC_N As Integer
    Public _FAL_DESC_P As Integer
    Public _FAL_SEPT_N As Integer
    Public _FAL_SEPT_P As Integer
    Public _FAL_INC_N As Integer
    Public _FAL_INC_P As Integer
    Public _FAL_AUS_N As Integer
    Public _FAL_AUS_P As Integer
    Public _FAL_INFO_N As Integer
    Public _FAL_INFO_P As Integer
    Public _FAL_PTU_N As Integer
    Public _FAL_PTU_P As Integer
    Public _TOTAL1 As Double
    Public _TOTAL2 As Double
    Public _TOTAL3 As Double
    Public _TOTAL4 As Double
    Public _TIPO_DESC As String
    Public _CANT_DESC As Double
    Public _F_INI_CTO As Date
    Public _F_VENC_CTO As Date
    Public _EMAIL As String
    Public _TELEFONO2 As String
    Public _DECLARA As String
    Public _TABLA_VAC As Integer
    Public _TIPOEXC As String
    Public _NIVEL_ESTUDIOS As Integer
    Public _PROFESION As String
    Public _ENFERMEDADES As String
    Public _NOMBRE_EMERGENCIA As String
    Public _DIRECCION_EMERGENCIA As String
    Public _TEL_EMERGENCIA As String
    Public _CAPACITACIONES As String
    Public _AREAGEO As String
    Public _OBSERVACION As String
    Public _TIPOJORNADA As Integer
    Public _CAL_SEPT_DIA As String
    Public _REG_PAT As Integer
    Public _TIPO_JORNADALA As String
    Public _SUELDOXHORA As Double
    Public _TOTALHOXDIA As Double
    Public _TDIASPERIODO As Double
    Public _TIPO_INGRESOS As Integer
    Public _CANTIDAD_POR_INGRESO As Double
    Public _PAIS As String
    Public _CUENTA_COI As String

    Public Sub clean()

        If Me._CUENTA_COI <> Nothing Then
            Me._CUENTA_COI = Me._CUENTA_COI.Trim()
        End If

        If Me._NOMBRE <> Nothing Then

            Me._NOMBRE = Me._NOMBRE.Trim()
            Me._NOMBRE = Functions.truncateStringToLength(Me._NOMBRE, 20)

        End If

        If Me._AP_PAT_ <> Nothing Then

            Me._AP_PAT_ = Me._AP_PAT_.Trim()
            Me._AP_PAT_ = Functions.truncateStringToLength(Me._AP_PAT_, 20)

        End If

        If Me._AP_MAT_ <> Nothing Then

            Me._AP_MAT_ = Me._AP_MAT_.Trim()
            Me._AP_MAT_ = Functions.truncateStringToLength(Me._AP_MAT_, 20)

        End If

        If Me._STATUS <> Nothing Then
            Me._STATUS = Me._STATUS.Trim()
            Me._STATUS = Functions.truncateStringToLength(Me._STATUS, 1)
        End If

        If Me._IMSS <> Nothing Then
            Me._IMSS = Me._IMSS.Trim()
            Me._IMSS = Functions.truncateStringToLength(Me._IMSS, 15)
        End If

        If Me._CURP <> Nothing Then
            Me._CURP = Me._CURP.Trim()
            Me._CURP = Functions.truncateStringToLength(Me._CURP, 18)
        End If
        If Me._SEXO <> Nothing Then
            Me._SEXO = Me._SEXO.Trim()
            Me._SEXO = Functions.truncateStringToLength(Me._SEXO, 1)
        End If

        If Me._PUESTO <> Nothing Then

            Me._PUESTO = Me._PUESTO.Trim()
            Me._PUESTO = Functions.truncateStringToLength(Me._PUESTO, 5)

            If Me._PUESTO <> Constants.EMPTY_STR Then

                Me._PUESTO = Me._PUESTO.PadLeft(5)

            End If

        End If

        If Me._PROFESION <> Nothing Then
            Me._PROFESION = Me._PROFESION.Trim()
            Me._PROFESION = Functions.truncateStringToLength(Me._PROFESION, 60)
        End If

        If Me._CTACHEQNOM <> Nothing Then
            Me._CTACHEQNOM = Me._CTACHEQNOM.Trim()
            Me._CTACHEQNOM = Functions.truncateStringToLength(Me._CTACHEQNOM, 16)
        End If

        If Me._ENT_FED <> Nothing Then
            Me._ENT_FED = Me._ENT_FED.Trim()
            Me._ENT_FED = Functions.truncateStringToLength(Me._ENT_FED, 2)
        End If

        If Me._LUG_NACIM <> Nothing Then
            Me._LUG_NACIM = Me._LUG_NACIM.Trim()
            Me._LUG_NACIM = Functions.truncateStringToLength(Me._LUG_NACIM, 2)
        End If

        If Me._CD_POBLAC <> Nothing Then
            Me._CD_POBLAC = Me._CD_POBLAC.Trim()
            Me._CD_POBLAC = Functions.truncateStringToLength(Me._CD_POBLAC, 35)
        End If

        If Me._COLONIA <> Nothing Then
            Me._COLONIA = Me._COLONIA.Trim()
            Me._COLONIA = Functions.truncateStringToLength(Me._COLONIA, 25)
        End If

        If Me._COD_POST <> Nothing Then
            Me._COD_POST = Me._COD_POST.Trim()
            Me._COD_POST = Functions.truncateStringToLength(Me._COD_POST, 5)
        End If

        If Me._NOM_PADRE <> Nothing Then
            Me._NOM_PADRE = Me._NOM_PADRE.Trim()
            Me._NOM_PADRE = Functions.truncateStringToLength(Me._NOM_PADRE, 60)
        End If

        If Me._NOM_MADRE <> Nothing Then
            Me._NOM_MADRE = Me._NOM_MADRE.Trim()
            Me._NOM_MADRE = Functions.truncateStringToLength(Me._NOM_MADRE, 60)
        End If

        If Me._CALLE <> Nothing Then
            Me._CALLE = Me._CALLE.Trim()
            Me._CALLE = Functions.truncateStringToLength(Me._CALLE, 40)
        End If

        If Me._TELEFONO <> Nothing Then
            Me._TELEFONO = Me._TELEFONO.Trim()
            Me._TELEFONO = Functions.truncateStringToLength(Me._TELEFONO, 16)
        End If

        If Me._EMAIL <> Nothing Then
            Me._EMAIL = Me._EMAIL.Trim()
            Me._EMAIL = Functions.truncateStringToLength(Me._EMAIL, 60)
        End If

    End Sub

End Class
