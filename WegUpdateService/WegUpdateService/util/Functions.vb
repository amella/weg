﻿Imports System.IO

Public Class Functions

    Shared Function truncateStringToLength(ByRef str As String, ByVal ln As Integer)

        If str <> Constants.EMPTY_STR And str.Length() > ln Then

            Return str.Substring(0, ln)

        Else

            Return str

        End If

    End Function

    Shared Sub writeToLog(ByRef foo As String, ByRef ruta As String)

        Dim sw As IO.StreamWriter = Nothing

        Try

            If IO.File.Exists(ruta) = True Then

                sw = File.AppendText(ruta)
                sw.WriteLine(foo)
                sw.Flush()

            Else

                sw = New IO.StreamWriter(ruta)
                sw.WriteLine(foo)
                sw.Flush()

            End If

        Catch ex As Exception

            Throw ex

        Finally

            If (Not sw Is Nothing) Then
                sw.Close()
            End If

        End Try
    End Sub

    Shared Sub writeToError(ByRef foo As String)

        Dim _now As DateTime = DateTime.Now()
        Dim name As String = Servicio._log & "error" & _now.Day & _now.Month & _now.Year & ".txt"
        Dim sw As IO.StreamWriter = Nothing

        Try

            If IO.File.Exists(name) = True Then

                sw = File.AppendText(name)
                sw.WriteLine(foo)
                sw.Flush()

            Else

                sw = New IO.StreamWriter(name)
                sw.WriteLine(foo)
                sw.Flush()

            End If

        Catch ex As Exception

            Throw ex

        Finally

            If (Not sw Is Nothing) Then
                sw.Close()
            End If

        End Try

    End Sub

End Class
