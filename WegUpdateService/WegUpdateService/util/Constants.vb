﻿Public Class Constants

    Public Const EMPTY_STR As String = ""
    Public Const PIPE_CHAR As String = "|"
    Public Const SEMI_COLON_CHAR As String = ";"
    Public Const POINT_CHAR As String = "."
    Public Const COMMA_CHAR As String = ","
    Public Const SLASH_CHAR As String = "/"

End Class
