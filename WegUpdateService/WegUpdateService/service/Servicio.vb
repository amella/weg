﻿Imports System.Net.Mail
Imports System.Globalization
Imports System.Threading

Public Class Servicio

    Public _trabajadorDAO As TrabajadorDAO
    Public _strRoute As String
    Public _total As Integer
    Public _now As DateTime
    Public Shared _log As String

    Public Function getTrabajadores()

        Dim fileReader As System.IO.StreamReader
        Dim encodingISO88591 As Text.Encoding = Text.Encoding.GetEncoding(28591)     
      
        fileReader = New System.IO.StreamReader(_strRoute, encodingISO88591)
      
        Dim linea As String

        Dim trabajadores As List(Of Trabajador) = New List(Of Trabajador)

        Dim trabajadorVO As Trabajador

        Do Until fileReader.EndOfStream

            linea = fileReader.ReadLine()

            Dim content() As String = Split(linea, Constants.SEMI_COLON_CHAR)

            trabajadorVO = New Trabajador()

            Dim clave As String = Integer.Parse(content(0))
            clave = clave.Trim()
            trabajadorVO._CLAVE = clave.PadLeft(10) 'Clave del empleado - Alineada a la izquierda 10 caracteres

            trabajadorVO._NOMBRE = content(1) 'Nombre del empleado
            trabajadorVO._AP_PAT_ = content(2) 'Apellido paterno del empleado
            trabajadorVO._AP_MAT_ = content(3) 'Apellido materno del empleado
            trabajadorVO._FECH_ALTA = Me.getDateFromString(content(4)) 'Fecha de alta del empleado dd/MM/yyyy
            trabajadorVO._STATUS = content(5) 'Status del empleado, A:Alta, B:Baja
            trabajadorVO._IMSS = content(6) 'Número de seguridad social
            trabajadorVO._CURP = content(7) 'CURP del empleado
            trabajadorVO._SEXO = content(8) 'Sexo del empleado
            trabajadorVO._PUESTO = content(9) 'Puesto del empleado
            trabajadorVO._PROFESION = content(10) 'Profesión del empleado
            trabajadorVO._FECH_NACIM = Me.getDateFromString(content(11)) 'Fecha de nacimiento del empleado
            trabajadorVO._CUENTA_COI = content(12) 'Cuenta Aspel-COI
            trabajadorVO._SAL_DIARIO = Me.getSalarioFromString(content(13)) 'Salario diario  
            Dim bancNomStr = content(14) 'Banco operador
            bancNomStr = bancNomStr.Trim()

            If bancNomStr <> Nothing Then

                bancNomStr = bancNomStr.Trim()

                If bancNomStr <> Constants.EMPTY_STR Then

                    trabajadorVO._BANC_NOM = Integer.Parse(bancNomStr)

                End If

            End If

            trabajadorVO._CTACHEQNOM = content(15) 'Cuenta de depósito del empleado
            trabajadorVO._ENT_FED = content(16) 'Entidad Federativa
            trabajadorVO._CD_POBLAC = content(17)  'Población
            trabajadorVO._COLONIA = content(18) 'Colonia del empleado
            trabajadorVO._COD_POST = content(19) 'Código postal
            trabajadorVO._CALLE = content(20) 'Calle y número
            trabajadorVO._LUG_NACIM = content(21) 'Lugar de nacimiento puede venir vacio
            trabajadorVO._NOM_PADRE = content(22) 'Nombre del padre
            trabajadorVO._NOM_MADRE = content(23) 'Nombre de la madre
            trabajadorVO._FECH_BAJA = Me.getDateFromString(content(24)) 'Fecha de baja del empleado dd/MM/yyyy
            trabajadorVO._TELEFONO = content(25) 'Teléfono del empleado
            trabajadorVO._EMAIL = content(26) 'Email del empleado

            trabajadorVO._SDI = Me.getSalarioFromString(content(27)) 'Salario diario cpturado

            trabajadorVO.clean()

            trabajadores.Add(trabajadorVO)

        Loop

        Return trabajadores

    End Function

    Public Sub doUpdateTrabajadores(ByRef trabajadores As List(Of Trabajador))

        _trabajadorDAO.updateTrabajadores(trabajadores)

    End Sub

    Public Sub doInsertTrabajadores(ByRef trabajadores As List(Of Trabajador))

        _trabajadorDAO.insertTrabajadores(trabajadores)

    End Sub

    Public Sub doUpdateFamilias(ByRef familias As List(Of Familia))
        _trabajadorDAO.updateFamilias(familias)
    End Sub

    Public Sub doInsertFamilias(ByRef familias As List(Of Familia))
        _trabajadorDAO.insertFamilias(familias)
    End Sub

    Public Sub getCorrectClaves(ByRef trabajadores As List(Of Trabajador))

        For Each t As Trabajador In trabajadores

            Dim clave As String = _trabajadorDAO.getCorrectClave(t)
            t._CLAVE = clave

        Next

    End Sub

    Public Function getFamiliasNuevos(ByRef familias As List(Of Familia))
        Return _trabajadorDAO.getFamiliasNuevos(familias)
    End Function

    Public Function getFamiliasExistentes(ByRef familias As List(Of Familia))
        Return _trabajadorDAO.getFamiliasExistentes(familias)
    End Function

    Public Function getTrabajadoresExistentes(ByRef trabajadores As List(Of Trabajador))
        Return _trabajadorDAO.getTrabajadoresExistentes(trabajadores)
    End Function

    Public Function getTrabajadoresNuevos(ByRef trabajadores As List(Of Trabajador))
        Return _trabajadorDAO.getTrabajadoresNuevos(trabajadores)
    End Function

    Private Function makeBody(ByRef archivos As List(Of Archivo))

        Dim foo As String = ""

        foo = foo & "Hora de ejecución: " & _now & vbCrLf & vbCrLf

        For Each archivo As Archivo In archivos

            foo = foo & "Archivo: " & archivo._ubicacion & vbCrLf & vbCrLf
            foo = foo & "Registros en el archivo: " & archivo._totalUpdate & vbCrLf & vbCrLf

            For Each empresa As Empresa In archivo._empresas

                foo = foo & "Empresa: " & empresa._id & vbCrLf
                foo = foo & "Fecha de afectación: " & empresa._actualDateEmpresa & vbCrLf
                foo = foo & "Registros: " & empresa._total & vbCrLf
                foo = foo & "Registros actualizados: " & empresa._totalUpdate & vbCrLf
                foo = foo & "Registros nuevos:" & empresa._totalInsert & vbCrLf & vbCrLf

            Next

            foo = foo & vbCrLf

        Next

        Return foo

    End Function

    Public Sub writeToFile(ByRef archivos As List(Of Archivo), ByRef mailSettings As MailSettings)

        Dim ruta As String = "log" & _now.Day & _now.Month & _now.Year & _now.Hour & _now.Minute & _now.Second

        Dim foo As String = Me.makeBody(archivos)

        Functions.writeToLog(foo, _log & ruta & ".txt")

    End Sub

    Public Sub sendmail(ByRef archivos As List(Of Archivo), ByRef mailSettings As MailSettings)

        Dim mailMessage As New System.Net.Mail.MailMessage()
        mailMessage.From = New System.Net.Mail.MailAddress(mailSettings._from)

        For Each two As String In mailSettings._tos
            mailMessage.To.Add(two)
        Next

        mailMessage.Subject = mailSettings._subject

        Dim msg As String = Me.makeBody(archivos)

        Try

            Dim _now As DateTime = DateTime.Now()
            Dim name As String = Servicio._log & "error" & _now.Day & _now.Month & _now.Year & ".txt"

            If IO.File.Exists(name) = True Then

                Dim fr As String = My.Computer.FileSystem.ReadAllText(name)

                If fr <> Nothing Then

                    msg = msg & vbCrLf & vbCrLf
                    msg = msg & "Errores" & vbCrLf
                    msg = msg & fr

                End If

            End If

        Catch ex As Exception

            Functions.writeToError("sendmail::Exception::message::" & ex.Message)

        End Try

        mailMessage.Body = msg
        mailMessage.Priority = MailPriority.Normal

        Dim sendMail As New Net.Mail.SmtpClient()
        sendMail.Host = mailSettings._host
        sendMail.EnableSsl = True
        sendMail.UseDefaultCredentials = False
        sendMail.Port = mailSettings._port
        sendMail.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
        sendMail.Credentials = New Net.NetworkCredential(mailSettings._user, mailSettings._password)
        sendMail.Timeout = mailSettings._timeout

        Try

            sendMail.Send(mailMessage)

        Catch ex As Exception

            Functions.writeToError("sendmail::Exception::message::" & ex.Message)            

        End Try


    End Sub


    Private Function getDateFromString(ByRef foo As String)

        Dim now As DateTime = Nothing

        If foo <> Nothing Then

            foo = foo.Trim()

            If foo = "00/00/0000" Then

                Return Nothing

            End If

            If foo <> Constants.EMPTY_STR Then

                Dim content() As String = Split(foo, Constants.SLASH_CHAR)

                Try

                    now = New DateTime(content(2), content(1), content(0), 0, 0, 0)

                    Return now.Date

                Catch ex As Exception

                    Functions.writeToError("getDateFromString::Exception::message::" & ex.Message)

                End Try

            End If

        End If

        Return Nothing

    End Function

    Public Sub getDeptos(ByRef trabajadores As List(Of Trabajador))

        For Each trabajdor As Trabajador In trabajadores

            trabajdor._DEPTO = _trabajadorDAO.getDepto(trabajdor)

        Next

    End Sub



    Private Function getSalarioFromString(ByRef foo As String)

        Dim salario As String = foo

        Dim salarioDouble = Nothing

        If salario <> Nothing Then

            salario = salario.Trim()

            If salario <> Constants.EMPTY_STR Then

                salario = Replace(salario, Constants.COMMA_CHAR, Constants.PIPE_CHAR)
                salario = Replace(salario, Constants.POINT_CHAR, Constants.EMPTY_STR)
                salario = Replace(salario, Constants.PIPE_CHAR, Constants.POINT_CHAR)

                salarioDouble = Double.Parse(salario)

            End If

        End If

        Return salarioDouble

    End Function

End Class
